extern crate globset;
extern crate image;
extern crate img_hash;
extern crate log;
extern crate rayon;
extern crate walkdir;

use globset::{GlobBuilder, GlobMatcher};
use img_hash::{Hasher, HasherConfig, ImageHash};
use log::{debug, error};
use rayon::prelude::*;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use walkdir::{DirEntry, WalkDir};

type HashSize = [u8; 8];

#[macro_export]
macro_rules! DEFAULT_FILTER {
    () => {
        "*.{jpg,jpeg,png,gif,webp,bmp,tiff}"
    };
}

pub struct HashOf {
    hash: ImageHash<HashSize>,
    file: PathBuf,
}

fn filter_glob_errors<E: std::fmt::Debug>(file: Result<DirEntry, E>) -> Option<PathBuf> {
    match file {
        Ok(file) => {
            if file.file_type().is_dir() {
                None
            } else {
                Some(file.into_path())
            }
        }
        Err(e) => {
            error!("Error during file matching: {:?}", e);
            None
        }
    }
}

fn hash_image(hasher: &mut Hasher<HashSize>, file: PathBuf) -> Option<HashOf> {
    match image::open(&file) {
        Ok(img) => {
            let hash = hasher.hash_image(&img);
            debug!("{:?} -> {}", file, hash.to_base64());

            Some(HashOf { hash, file })
        }
        Err(e) => {
            error!("Error decoding image {:?}: {:?}", file, e);
            None
        }
    }
}

pub fn calculate_hashes_sequential(tree: WalkDir, matcher: GlobMatcher) -> Vec<HashOf> {
    let mut hasher = HasherConfig::with_bytes_type::<HashSize>().to_hasher();

    tree.into_iter()
        .filter_map(filter_glob_errors)
        .filter(|file| matcher.is_match(file))
        .filter_map(|file| hash_image(&mut hasher, file))
        .collect()
}

pub fn calculate_hashes_parallel(tree: WalkDir, matcher: GlobMatcher) -> Vec<HashOf> {
    tree.into_iter()
        .filter_map(filter_glob_errors)
        .filter(|file| matcher.is_match(file))
        .par_bridge()
        .map_init(
            || HasherConfig::with_bytes_type::<HashSize>().to_hasher(),
            hash_image,
        )
        .filter_map(|hash| hash)
        .collect()
}

pub fn calculate_hashes_for_dir(
    root: &Path,
    parallel: bool,
    custom_filter: Option<&str>,
) -> Vec<HashOf> {
    let images = WalkDir::new(root);

    let glob = GlobBuilder::new(custom_filter.unwrap_or(DEFAULT_FILTER!()))
        .case_insensitive(true)
        .build()
        .expect("Failed to parse glob pattern")
        .compile_matcher();

    if parallel {
        calculate_hashes_parallel(images, glob)
    } else {
        calculate_hashes_sequential(images, glob)
    }
}

pub fn calculate_hashes_for_targets(targets: impl Iterator<Item = PathBuf>) -> Vec<HashOf> {
    let mut hasher = HasherConfig::with_bytes_type::<HashSize>().to_hasher();
    targets
        .filter_map(|path| hash_image(&mut hasher, path))
        .collect()
}

pub fn duplicates_for_all(hashes: &[HashOf], threshold: u32) -> HashMap<&Path, Vec<&Path>> {
    let mut table: HashMap<&Path, Vec<&Path>> = HashMap::new();

    'outer: for (i, a) in hashes.iter().enumerate() {
        for dups in table.values() {
            if dups.contains(&a.file.as_path()) {
                continue 'outer;
            }
        }

        for b in hashes.iter().skip(i + 1) {
            let dist = a.hash.dist(&b.hash);

            if dist <= threshold {
                let dups = table.entry(&a.file).or_insert_with(Vec::new);
                dups.push(&b.file)
            }
        }
    }

    table
}

pub fn duplicates_for_targets<'a>(
    targets: &'a [HashOf],
    pool: &'a [HashOf],
    threshold: u32,
) -> HashMap<&'a Path, Vec<&'a Path>> {
    let mut table: HashMap<&Path, Vec<&Path>> = HashMap::new();

    for target in targets {
        for image in pool {
            let dist = target.hash.dist(&image.hash);

            if dist <= threshold {
                let dups = table.entry(&target.file).or_insert_with(Vec::new);
                if Path::new(&target.file).canonicalize().unwrap()
                    != Path::new(&image.file).canonicalize().unwrap()
                {
                    dups.push(&image.file)
                }
            }
        }
    }
    table
}
