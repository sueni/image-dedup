extern crate clap;
extern crate globset;
extern crate image_dedup;
extern crate log;
extern crate stderrlog;
extern crate walkdir;

use clap::clap_app;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use stderrlog::Timestamp;

static FILTER_HELP: &str = concat!(
    "Filename pattern filter. Default: ",
    image_dedup::DEFAULT_FILTER!()
);

fn main() -> Result<(), std::io::Error> {
    let matches = clap_app!(myapp =>
        (@arg filter: -e --filter FILTER_HELP)
        (@arg sequential: -s --seq "Turn off parallel execution - execution will be slower but somewhat less IO-intensive")
        (@arg threshold: -t --threshold possible_value[low hi] +takes_value default_value("low") "Threshold in similarity")
        (@arg verbose: -v --verbose "Print file hashes")
        (@arg dir: -d --directory default_value(".") "Sets the root directory, where images will be searched")
        (@arg targets: ... "Search duplicates for the specified image only")
    ).get_matches();

    let dir = Path::new(matches.value_of("dir").unwrap());
    if !dir.is_dir() {
        panic!("The given path is not a directory!");
    }

    stderrlog::new()
        .module(module_path!())
        .quiet(!matches.is_present("verbose"))
        .verbosity(3)
        .timestamp(Timestamp::Off)
        .init()
        .unwrap();

    let hashes = image_dedup::calculate_hashes_for_dir(
        dir,
        !matches.is_present("sequential"),
        matches.value_of("filter"),
    );

    let threshold = match matches.value_of("threshold") {
        Some("low") => 5,
        Some("hi") => 20,
        Some(_) => panic!("Impossible"),
        None => panic!("Impossible"),
    };

    let hashed_targets = match matches.values_of("targets") {
        Some(files) => {
            let targets = files.map(|f| Path::new(f).to_owned());
            image_dedup::calculate_hashes_for_targets(targets)
        }
        _ => Vec::new(),
    };

    let table = if hashed_targets.is_empty() {
        image_dedup::duplicates_for_all(&hashes, threshold)
    } else {
        image_dedup::duplicates_for_targets(&hashed_targets, &hashes, threshold)
    };

    if table.is_empty() {
        return Ok(());
    }

    let stdout = std::io::stdout();
    let mut handle = stdout.lock();

    for (file, dups) in table.iter() {
        handle.write_all(file.as_os_str().as_bytes())?;
        handle.write_all(b"\0")?;

        for dup in dups {
            handle.write_all(dup.as_os_str().as_bytes())?;
            handle.write_all(b"\0")?;
        }

        handle.write_all(b"\n")?;
    }
    Ok(())
}
